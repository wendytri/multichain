### Purpose ###
Make a simple simulation of voting system using Multichain. The simulation involves two machines (in my case, two virtual machines with the same Ubuntu OS).

### Summary ###
The system works as follow:

1. Each voter is mailed a card with an unique random ID number.

2. The voter installs the blockchain software and joins the blockchain.

3. The voter goes to a faucet and gets a token, using the ID number. Each ID number is only allowed to get one token.

4. The voter can vote online by sending the token to the account of the candidate they prefer. That voter cannot vote again, but the voter can examine the blockchain to verify that the vote was correctly recorded, and also see the total votes for each candidate at any time.

5. If a voter can't or won't vote online, the voter can go in person to a polling place. Every polling place has the same blockchain, so they can verify that the voter has not yet voted. If the voter lost the card, or never received it, the polling place can verify that the token has not already voted, invalidate it, and assign that person a new token.
Mining will be done by machines that are chosen somehow to represent all stakeholders, such as one miner per candidate, or per political party, or per county.

### Setup ###
**Let the first machine be the Poll Server
**
```
#!HTML

1. Install Multichain on the Poll Server
   cd /tmp
   wget http://www.multichain.com/download/multichain-1.0-alpha-21.tar.gz
   tar -xvzf multichain-1.0-alpha-21.tar.gz
   cd multichain-1.0-alpha-21
   sudo mv multichaind multichain-cli multichain-util /usr/local/bin 

2. Create a Blockchain on the Poll Server
   First we will create a new blockchain named "survey"
    multichain-util create survey

3. Adjusting Blockchain Settings
   nano ~/.multichain/survey/params.dat

   In the "Global permissions" section, change these four parameters, as shown below.
   anyone-can-connect = true 
   anyone-can-send = true
   allow-p2sh-outputs = false 
   allow-multisig-outputs = false
   In the "Consensus requirements" section, change this parameter, as shown below.
   setup-first-blocks = 10000
   Save the file with Ctrl+X, Y, Enter.

4. Initialise the Blockchain
   multichaind survey -daemon
   Make a note of your node address
   
5. Getting an Address on the Poll Server
   multichain-cli survey getnewaddress
   Make a note of it.

6. Giving the Poll Server Address Send and Receive Permission
   multichain-cli survey grant Poll_Server_Address receive,send

7. Issue the "token" Asset
   Now, create an asset named "token" which cannot be subdivided, and place 10000 of them on the Poll Server
   multichain-cli survey issue Poll_Server_Address token 10000 1
   multichain-cli survey listassests

8. Install Apache on the Poll Server
   sudo apt-get update
   sudo apt-get install apache
   sudo service apache2 start
   
9. Install PHP on the Poll Server
   sudo apt-get install libapache2-mod-php

10. Find your RPC Credentials: username and password
    cat ~/.multichain/survey/multichain.conf
	Make a note of them.

11. Make info.php
    sudo nano /var/www/html/info.php
    Enter or paste in this code. You will need to change two items:
    1. Replace the password in the line beginning with "$a" with the correct password on your Poll Server 
    2. Replace the port number at the end of the line beginning with "$c" with the actual port number on your server.

     <?php
     echo "<h1>Information About the Survey Blockchain</h1>";

     $a = 'curl -s --user multichainrpc:FHtgQdshp7bo96gNv4MtT64cQwj1inTQAEuonr5jG7hv --data-binary \'';
     $b = '{"jsonrpc": "1.0", "id":"curltest", "method": "getinfo", "params": [';
     $c = '] }\' -H "content-type: text/plain;" http://127.0.0.1:7412/';
     $cmd = $a . $b . $c;

     echo "\n<h2>Raw Output</h2><pre>\n";
     $ret=system($cmd);

     echo "\n<h2>Decoded Output</h2>\n";
     $rets = json_decode($ret, true);
     print_r($rets);

     echo "\n<h2>Single-Item Output</h2>\n";
     echo $rets['result']['version'];
     ?>

    Save the file with Ctrl+X, Y, Enter.
	To test the script, execute:
	php /var/www/html/info.php
	
12. Make a pay.php Script (this script will send 1 token to a specific address)
    sudo nano /var/www/html/pay.php

    Enter or paste in this code. You will need to change two items:
	
    1. Replace the password in the line beginning with "$a" with the correct password on your Poll Server 
    2. Replace the port number at the end of the line beginning with "$d" with the actual port number on your server.

    <?php

    echo "<h1>Sending you a Survey Token!</h1>";
    $addr = $_POST["address"];

    $a = 'curl -s --user multichainrpc:FHtgQdshp7bo96gNv4MtT64cQwj1inTQAEuonr5jG7hv --data-binary \'';
    $b = '{"jsonrpc": "1.0", "id":"curltest", "method": "sendassettoaddress", "params": ["';
    $c = '", "token", 1';
    $d = '] }\' -H "content-type: text/plain;" http://127.0.0.1:7412/';
    $cmd = $a . $b . $addr . $c . $d;

    $ret=system($cmd);
    echo $ret;
    ?>
	
	Save the file with Ctrl+X, Y, Enter.

13. Make a faucet.htm Page
    sudo nano /var/www/html/faucet.htm
	
    Enter or paste in this code.
    <html><head><title>Survey Token Faucet</title></head>
    <body bgcolor="#cccccc">
    <h1 align="center">Survey Token Faucet</h1>

    <form method="post" action="pay.php">
    <p align="center"><b>Enter your address</b></p>
    <p align="center"><input type="text" name="address" size="90"></textarea></p>
    <p align="center">
    <button type="submit" name="submitButton" value="">Get Token</button>
    </form> 

    </body></html>
	
	Save the file with Ctrl+X, Y, Enter.
```


**Let the second machine be the Voter
**
```
#!HTML

1.  Install Multichain
    cd /tmp
    wget http://www.multichain.com/download/multichain-1.0-alpha-21.tar.gz
    tar -xvzf multichain-1.0-alpha-21.tar.gz
    cd multichain-1.0-alpha-21
    sudo mv multichaind multichain-cli multichain-util /usr/local/bin

2. Connecting the Voter to the Blockchain
   multichaind node_address -daemon
  
3. Getting an address
   multichain-cli survey getnewaddress
   Make a note of it.

4. Using the Faucet
   On the Voter, open a Web browser and go to this address, replacing the IP address with the IP address of your Poll Server.
   You used this address just a few steps above to connect to the blockchain.
   Poll_Server_Address/faucet.htm
   Paste your address into the Faucet page.
   Click the "Get Token" button. An error message appears, showing the address doesn't have permission.

**Voting   
**1. Make pay2.php (Machine #1)
   We don't want Voters sending tokens to one another, so we don't want the Voters to have receive permission all the time.
   What we really want is to grant the Voter receive permission just long enough to receive 1 token, and then revoke it.

   Looking at the MultiChain JSON-RPC API commands reveals that there are commands to do precisely that.
   sudo nano /var/www/html/pay2.php
   
   Enter or paste in this code.
   <?php
    echo "<h1>Sending you a Survey Token!</h1>";
    $addr = $_POST["address"];

    $a = 'curl -s --user multichainrpc:FHtgQdshp7bo96gNv4MtT64cQwj1inTQAEuonr5jG7hv --data-binary \'';
    $b = '{"jsonrpc": "1.0", "id":"curltest", "method": "grant", "params": ["';
    $c = '", "receive"';
    $d = '] }\' -H "content-type: text/plain;" http://127.0.0.1:7412/';
    $cmd = $a . $b . $addr . $c . $d;
    $ret=system($cmd);

    $a = 'curl -s --user multichainrpc:FHtgQdshp7bo96gNv4MtT64cQwj1inTQAEuonr5jG7hv --data-binary \'';
    $b = '{"jsonrpc": "1.0", "id":"curltest", "method": "sendassettoaddress", "params": ["';
    $c = '", "token", 1';
    $d = '] }\' -H "content-type: text/plain;" http://127.0.0.1:7412/';
    $cmd = $a . $b . $addr . $c . $d;
    $ret=system($cmd);

    $a = 'curl -s --user multichainrpc:FHtgQdshp7bo96gNv4MtT64cQwj1inTQAEuonr5jG7hv --data-binary \'';
    $b = '{"jsonrpc": "1.0", "id":"curltest", "method": "revoke", "params": ["';
    $c = '", "receive"';
    $d = '] }\' -H "content-type: text/plain;" http://127.0.0.1:7412/';
    $cmd = $a . $b . $addr . $c . $d;
    $ret=system($cmd);
    ?>

    Save the file with Ctrl+X, Y, Enter.
	
2. Editing the faucet.htm Page (Machine #1)
   The faucet.htm file must be modified to reference the pay2.php script.
   sudo nano /var/www/html/faucet.htm

   <html><head><title>Survey Token Faucet</title></head>
   <body bgcolor="#cccccc">
   <h1 align="center">Survey Token Faucet</h1>

   <form method="post" action="pay2.php">
   <p align="center"><b>Enter your address</b></p>
   <p align="center"><input type="text" name="address" size="90"></textarea></p>
   <p align="center">
   <button type="submit" name="submitButton" value="">Get Token</button>
   </form> 

   </body></html>
	
   Save the file with Ctrl+X, Y, Enter.

3. Using the Faucet (Machine #2)
   On the Voter, in your Web browser, click the Back button to return to the Faucet. Click the Refresh button to reload the page.
   Paste in your address.
   Click the "Get Token" button.
   Three messages appear, all showing "error:null"
   
4. Checking your Balance (Machine #2)
   multichain-cli survey gettotalbalances
   You now have 1 token.

5. Getting an Account for Candidate #1 (Machine #1)
   multichain-cli survey getnewaddress
   multichain-cli survey grant new_address

6. Getting an Account for Candidate #2 (Machine #1)
   multichain-cli survey getnewaddress
   multichain-cli survey grant new_address
	
7. Making a Candidates Page (Machine #1)
   sudo nano /var/www/html/candidates.htm
	
   Paste in this code. Replace the addresses with the two addresses you just made.
   <html>
   <head><title>Candidates</title></head>
   <body>

   <h2>Candidate #1</h2>
   17CFf1ZJV73igpcb2egZRFjGYiabTf21T61kiQ

   <h2>Candidate #1</h2>
   1CTMCxciVnWCGorSzgErkRjA2ogDK7XF7nT1nG

   </body>
   </html>
	
    ave the file with Ctrl+X, Y, Enter.
	
8. Voting (Machine #2)
   On the Voter, in a Web browser, and go to this address, replacing the IP address with the IP address of your Poll Server.
   Poll_Server_Address/candidates.htm

   To vote, on your Voter Machine, execute this command,
   replacing the address with one of the addresses from the Candidates page.
   multichain-cli survey sendassettoaddress 17CFf1ZJV73igpcb2egZRFjGYiabTf21T61kiQ token 1

   To check your account balance, on your Voter Machine, execute this command,
   replacing the address with one of the addresses from the Candidates page.
   Your token is gone.

9. Viewing the Election Results (Machine #2)
   To see the election results, execute these commands on the Poll Server, replacing the addresses with the addresses of your candidates.
   multichain-cli survey getaddressbalances 17CFf1ZJV73igpcb2egZRFjGYiabTf21T61kiQ
   multichain-cli survey getaddressbalances 1CTMCxciVnWCGorSzgErkRjA2ogDK7XF7nT1nG
   One candidate has 1 token, and the other has no tokens.
   multichain-cli survey getmultibalances 17CFf1ZJV73igpcb2egZRFjGYiabTf21T61kiQ,1CTMCxciVnWCGorSzgErkRjA2ogDK7XF7nT1nG

```

Following each step, I got the same result as the tutorial indicated except for the addresses and port.

### Limitations ####
1. Same person can vote again (haven't got restrictions)
2. Voters can view the results independently

### Reference ###
https://samsclass.info/141/proj/pMult2.htm